/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')
const prettierConfig = require('./.prettierrc.js')

module.exports = {
  root: true,
  extends: [
    'plugin:import/typescript',
    'plugin:vue/essential',
    'eslint:recommended',
    '@vue/eslint-config-prettier',
    'prettier/prettier'
  ],
  plugins: ['simple-import-sort', 'prettier'],
  rules: {
    'prettier/prettier': ['error', prettierConfig],
    'simple-import-sort/imports': 'error',
    'simple-import-sort/exports': 'error',
    'spaced-comment': [
      'error',
      'always',
      {
        line: {
          markers: ['/'],
          exceptions: ['-', '+']
        },
        block: {
          markers: ['!'],
          exceptions: ['*'],
          balanced: true
        }
      }
    ],
    'linebreak-style': ['error', 'unix'],
    'no-empty-function': 'off'
  },
  overrides: [
    {
      files: ['cypress/e2e/**.{cy,spec}.{js,ts,jsx,tsx}'],
      extends: ['plugin:cypress/recommended']
    }
  ]
}
