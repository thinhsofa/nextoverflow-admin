import Vue from 'vue';
import i18n from '@/locales/i18n';
import vuetify from '@/plugins/vuetify';
import '@/plugins/vee-validate';

import App from './App.vue';
import router from './router';
import store from './store';
import iconPackage from './plugins/icon-package';

Vue.use(iconPackage);

import './assets/main.css';

new Vue({
  vuetify,
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
