import Vue from 'vue';
import Vuex from 'vuex';

import userStore from './user';
import usersManagerStore from './usersManager';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user: userStore,
    usersManager: usersManagerStore,
  },
});
