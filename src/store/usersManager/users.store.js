import { getUsersList } from '@/api/users-manager';

export default {
  namespaced: true,
  state: {
    usersList: [],
  },
  getters: {
    usersList(state) {
      return state.usersList;
    },
  },
  mutations: {
    GET_USERS_LIST(state, data) {
      state.usersList = data;
    },
  },
  actions: {
    async getUsersList(context) {
      const [error, response] = await getUsersList();
      if (!error && response) {
        context.commit('GET_USERS_LIST', response.data);
      } else {
        console.error(error);
      }
    },
  },
};
