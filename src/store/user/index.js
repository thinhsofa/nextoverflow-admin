export default {
  namespaced: true,
  state: {
    data: 'Hi',
  },
  mutations: {
    SET_DATA: (state, payload) => {
      state.data = payload;
    },
  },
  actions: {
    setData({ commit }, payload) {
      commit('SET_DATA', payload);
    },
  },
  getters: {
    getData(state) {
      return state.data;
    },
  },
};
