export const localeMapping = Object.freeze({
  en: 'English',
  vi: 'Vietnamese',
});

export const languagesMenu = [
  { title: 'English', value: 'en' },
  { title: 'Vietnamese', value: 'vi' },
];
