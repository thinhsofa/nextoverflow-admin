export const routes = Object.freeze({
  home: '/',
  me: 'me',
  users: 'users',
  questions: 'questions',
  tags: 'tags',
  configs: 'configs',
  auth: '/auth',
  authenticate: 'authenticate',
});

export const sidebarItems = [
  { title: 'sidebar.home', icon: 'mdi-home', href: routes.home },
  { title: 'sidebar.users', icon: 'mdi-account', href: routes.users },
  {
    title: 'sidebar.questions',
    icon: 'mdi-message-question',
    href: routes.questions,
  },
  { title: 'sidebar.tags', icon: 'mdi-tag', href: routes.tags },
  { title: 'sidebar.configs', icon: 'mdi-cogs', href: routes.configs },
];
