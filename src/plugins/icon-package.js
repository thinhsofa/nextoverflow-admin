import Home from '@/components/icons/IconHome.vue';
import Close from '@/components/icons/IconClose.vue';
import RightTriangle from '@/components/icons/IconRightTriangle.vue';
import Search from '@/components/icons/IconSearch.vue';

const iconElements = {
  Home,
  Close,
  RightTriangle,
  Search,
};

export default {
  install(Vue) {
    Object.keys(iconElements).forEach((compName) => {
      Vue.component(`icon-${compName.toLowerCase()}`, iconElements[compName]);
    });
  },
};
