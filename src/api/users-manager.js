import axiosInstance from './axios';

export const getUsersList = async () => {
  try {
    const { data } = await axiosInstance.get('/users');
    return [null, data];
  } catch (error) {
    return [error];
  }
};

export default {
  getUsersList,
};
