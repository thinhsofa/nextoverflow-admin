import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: 'http://18.136.64.128:3006/v1',
  headers: {
    'Content-Type': 'Application/json',
  },
});
axiosInstance.interceptors.request
  // config token
  .use();
axiosInstance.interceptors.response.use(
  (res) => res,
  (err) => Promise.reject(err)
);

export default axiosInstance;
