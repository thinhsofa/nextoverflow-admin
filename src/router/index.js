import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/views/home';
import LoginView from '@/views/auth';
import UsersView from '@/views/usersManager';
import MainLayout from '@/layouts/MainLayout';
import AuthLayout from '@/layouts/AuthLayout';
import { routes } from '@/configs/routes';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: import.meta.env.BASE_URL,
  routes: [
    {
      path: routes.home,
      component: MainLayout,
      children: [
        {
          path: routes.home,
          name: 'home',
          component: Home,
        },
        {
          path: routes.me,
          name: 'me',
          component: Home,
        },
        {
          path: routes.users,
          name: 'users',
          component: UsersView,
        },
        {
          path: routes.questions,
          name: 'questions',
          component: Home,
        },
        {
          path: routes.tags,
          name: 'tags',
          component: Home,
        },
        {
          path: routes.configs,
          name: 'configs',
          component: Home,
        },
      ],
    },
    {
      path: routes.auth,
      component: AuthLayout,
      children: [
        {
          path: routes.authenticate,
          name: 'authenticate',
          component: LoginView,
        },
      ],
    },
  ],
});

export default router;
