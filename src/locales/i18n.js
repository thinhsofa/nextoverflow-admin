import Vue from 'vue';
import VueI18n from 'vue-i18n';

import en from './en';
import vi from './vi';

export const SUPPORTED_LOCALES = ['en', 'vi'];

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: { en, vi },
});

export default i18n;
