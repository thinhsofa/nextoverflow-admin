import sidebar from './sidebar.json';
import usersManager from './users-manager.json';
import common from './common.json';
import header from './header.json';

const en = { sidebar, usersManager, common, header };

export default en;
