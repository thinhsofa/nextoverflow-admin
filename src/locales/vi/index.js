import sidebar from './sidebar.json';
import usersManager from './users-manager.json';
import common from './common.json';
import header from './header.json';

const vi = { sidebar, usersManager, common, header };

export default vi;
